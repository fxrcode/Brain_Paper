import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon
import matplotlib.patches as mpatches

from pylab import *
import sys

plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 12
plt.rcParams['xtick.labelsize'] = 12


def rotateLabel(ax, deg):
    l = ax.get_xticklabels()
    plt.setp(l, rotation=deg)


# https://convert.town/column-to-comma-separated-list

B1_tra = np.array(
    [14.538, 17.408, 14.138, 17.971, 16.634, 16.96, 13.573, 14.617, 14.041, 14.479, 16.783, 20.85, 18.764])
B1_wotps = np.array(
    [9.6437, 3.7417, 13.342, 10.344, 11.358, 8.8318, 8.2462, 8.775, 7.4833, 15.556, 16.031, 5.831, 8.2462])
B1_tps = np.array(
    [6.7082, 6.4031, 13.379, 11.225, 11.045, 5.7446, 8.4853, 6.7082, 7.874, 12.124, 15.067, 0, 13.454])

B2_tra = np.array(
    [8.9518, 4.5959, 16.137, 33.089, 23.808, 19.525, 26.705, 30.473, 30.492, 37.558, 23.308])
B2_wotps = np.array(
    [9.9499, 16.031, 8.4853, 20.1, 7.0711, 9.0554, 8.124, 13.928, 9.6437, 7.2111, 2.2361])
B2_tps = np.array(
    [8.124, 16.492, 7.1414, 24.718, 6.4031, 4.1231, 9.798, 4.2426, 3.7417, 8.0623, 2.8284])

B3_tra = np.array(
    [16.512, 13.508, 20.773, 30.233, 25.328, 25, 35.269, 25.734, 31.311, 37.601, 30.157, 28.141])
B3_wotps = np.array(
    [11.446, 12.083, 7.1414, 16.912, 3.1623, 6.7082, 9.434, 13.601, 5.4772, 4.5826, 9.2736, 9.2736])
B3_tps = np.array(
    [7.8102, 12.083, 4.1231, 18.193, 8.0623, 7.874, 11.832, 2.8284, 8.544, 6.4031, 5.7446, 13.675, 7.6811])

B4_tra = np.array(
    [25.338, 23.363, 25.539, 15.853, 15.01, 26.336, 22.619, 27.114, 39.111, 25.364, 30.1])
B4_wotps = np.array(
    [17.234, 3.6056, 14.248, 7.2801, 7.874, 15.427, 12.689, 9.5394, 12.41, 7.1414, 5.831])
B4_tps = np.array(
    [6.4807, 4.4721, 17.72, 8.124, 8.0623, 11.402, 8.0623, 9.4868, 14.353, 7.874, 7.6811])

B5_tra = np.array(
    [24.301, 17.833, 25.803, 24.298, 29.696, 17.669, 25.82, 13.979, 13.461, 34.077, 18.952, 9.6219, 25.725, 23.966])
B5_wotps = np.array(
    [11.358, 13.964, 8.0623, 8.7178, 2.2361, 16.401, 15.166, 7.2801, 8.0623, 6.4807, 13.748, 15.033, 5.3852, 7.4833])
B5_tps = np.array(
    [11.832, 10.488, 2.4495, 7, 4.1231, 6.5574, 11.358, 10.344, 5.4772, 8, 6.0828, 11.358, 15.297, 5.3852])

B_label = ('tradition', 'w/o tps', 'tps', 'tradition', 'w/o tps', 'tps', 'tradition', 'w/o tps', 'tps',
           'tradition', 'w/o tps', 'tps', 'tradition', 'w/o tps', 'tps')
B_label_color = {'tradition': 'darkkhaki', 'w/o tps': 'orange', 'tps': 'royalblue'}
B1_data = [B1_tra, B1_wotps, B1_tps]
B2_data = [B2_tra, B2_wotps, B2_tps]
B3_data = [B3_tra, B3_wotps, B3_tps]
B4_data = [B4_tra, B4_wotps, B4_tps]
B5_data = [B5_tra, B5_wotps, B5_tps]
B_datas = [B1_tra, B1_wotps, B1_tps, B2_tra, B2_wotps, B2_tps, B3_tra, B3_wotps, B3_tps,
           B4_tra, B4_wotps, B4_tps, B5_tra, B5_wotps, B5_tps]

fig = plt.figure()
plt.subplots_adjust(hspace=0.01)

ax = plt.subplot2grid((1, 1), (0, 0))
bp = ax.boxplot(B_datas, labels=B_label, showfliers=False)

# https://matplotlib.org/examples/pylab_examples/boxplot_demo2.html
# Now fill the boxes with desired colors

boxColors = ['darkkhaki', 'orange', 'royalblue']
numBoxes = 5 * 3
medians = list(range(numBoxes))
for i in range(numBoxes):
    box = bp['boxes'][i]
    boxX = []
    boxY = []
    for j in range(5):
        boxX.append(box.get_xdata()[j])
        boxY.append(box.get_ydata()[j])
    boxCoords = list(zip(boxX, boxY))
    # Alternate between Dark Khaki and Royal Blue
    k = i % 3
    boxPolygon = Polygon(boxCoords, facecolor=boxColors[k])
    ax.add_patch(boxPolygon)
    # Now draw the median lines back over what we just filled in
    med = bp['medians'][i]
    medianX = []
    medianY = []
    for j in range(2):
        medianX.append(med.get_xdata()[j])
        medianY.append(med.get_ydata()[j])
        plt.plot(medianX, medianY, 'k')
        medians[i] = medianY[0]
        # Finally, overplot the sample averages, with horizontal alignment
        # in the center of each box
        plt.plot([np.average(med.get_xdata())], [np.average(B_datas[i])],
                 color='w', marker='*', markeredgecolor='k')

# box = ax.get_position()
# ax.set_position([box.x0, box.y0 + box.height * 0.1,
                 # box.width, box.height * 0.9])

# leg = legend([bp['boxes'][0],bp['boxes'][1],bp['boxes'][2]], ['yi','er','san'],
#        loc='upper center', bbox_to_anchor=(0.5, -0.05),
#                  fancybox=True, shadow=True, ncol=5)
#
# for color,text,line in zip(boxColors, leg.get_texts(), leg.get_lines()):
#     text.set_color(color)
#     line.set_linewidth(10)

realname = ['Reconstruction - first', 'Our method - general', 'Our method - Data specific improvement']
i = 0
for b in bp['boxes']:
    lab = ax.get_xticklabels()[i].get_text()
    print("Label property of box {0} is {1}".format(i, lab))
    b.set_color(B_label_color[lab])
    b.set_label(realname[i])
    i += 1
    if i == 3:
        break

leg = ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
                 fancybox=True, shadow=True, ncol=5, prop={'size': 11})

# https://stackoverflow.com/questions/13828246/matplotlib-text-color-code-in-the-legend-instead-of-a-line
for l in leg.legendHandles:
    l.set_linewidth(10)

plt.axvline(x=3.5, ls='-.')
plt.axvline(x=6.5, ls='-.')
plt.axvline(x=9.5, ls='-.')
plt.axvline(x=12.5, ls='-.')

# plt.setp(ax.get_xticklabels(), visible=False)


x1 = [2, 5, 8, 11, 14]
squad = ['Brain1', 'Brain2', 'Brain3', 'Brain4', 'Brain5']

ax.set_xticks(x1)
ax.set_xticklabels(squad, minor=False)

# Hide these grid behind plot objects
ax.set_axisbelow(True)
ax.set_title('TRE by Brain and Method')
# ax.set_xlabel('')
ax.set_ylabel('TRE (in pixels)')

# Finally, add a basic legend
# plt.figtext(0.80, 0.08, ' Random Numbers',
#             backgroundcolor=boxColors[0], color='black', weight='roman',
#             size='x-small')
# plt.figtext(0.80, 0.045, 'IID Bootstrap Resample',
#             backgroundcolor=boxColors[1],
#             color='white', weight='roman', size='x-small')
# plt.figtext(0.815, 0.013, ' Average Value', color='black', weight='roman',
#             size='x-small')

plt.tight_layout()
plt.show()
