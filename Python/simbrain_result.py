from pylab import *

plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 12
plt.rcParams['xtick.labelsize'] = 12


def rotateLabel(ax, deg):
    l = ax.get_xticklabels()
    plt.setp(l, rotation=deg)


# https://convert.town/column-to-comma-separated-list
Error_Meas = np.array(
    [1.6972, 2.2361, 11.18, 8.6023, 10.63, 10.77, 9.2195, 6.4031, 13.454, 9.1104, 17.263])

Comp_Meas_recon = np.array(
    [6.0343, 9.3894, 4.292, 5.0134, 17.837, 5.8477, 3.9433, 9.0947, 10.409, 3.7157, 8.3994, 7.3998, 8.8567, 8.3802,
     4.3193, 6.0676, 10.069, 10.001, 4.9021, 4.9837])

Comp_Meas_our = np.array(
    [1.4142, 1.7321, 1, 1.4142, 3.1623, 2.4495, 2.4495, 1.4142, 1.4142, 1, 2, 4.2426, 2.4495, 1.7321, 5.099, 2.2361,
     3.1623, 4.2426, 5.099, 0])

TRE_recon = np.array(
    [6.1334, 11.478, 17.486, 20.295, 4.7636, 17.505, 14.605, 10.354, 12.143, 20.062, 13.225])

TRE_our = np.array(
    [9.1652, 3.3166, 10.488, 1.4142, 8.0623, 13.342, 8.124, 3.7417, 11.045, 16.31, 14.629])

res_label = ('Error_Measure', 'Comp_Error_recon', 'Comp_Error_our', 'TRE_recon', 'TRE_our')
res_label_color = {'Comp_Error_recon': 'green', 'Comp_Error_our': 'royalblue'}

Error_data = [Error_Meas]
Comp_data = [Comp_Meas_recon, Comp_Meas_our]
TRE_data = [TRE_recon, TRE_our]
datas = [Error_Meas, Comp_Meas_recon, Comp_Meas_our, TRE_recon, TRE_our]

fig = plt.figure()
plt.subplots_adjust(hspace=0.01)

ax = plt.subplot2grid((1, 1), (0, 0))
bp = ax.boxplot(datas, labels=res_label, showfliers=False)

# https://matplotlib.org/examples/pylab_examples/boxplot_demo2.html
# Now fill the boxes with desired colors

boxColors = ['darkkhaki', 'green', 'royalblue', 'green', 'royalblue']
numBoxes = 5
medians = list(range(numBoxes))
for i in range(numBoxes):
    box = bp['boxes'][i]
    boxX = []
    boxY = []
    for j in range(5):
        boxX.append(box.get_xdata()[j])
        boxY.append(box.get_ydata()[j])
    boxCoords = list(zip(boxX, boxY))
    # Alternate between Dark Khaki and Royal Blue
    k = i % 5
    boxPolygon = Polygon(boxCoords, facecolor=boxColors[k])
    ax.add_patch(boxPolygon)
    # Now draw the median lines back over what we just filled in
    med = bp['medians'][i]
    medianX = []
    medianY = []
    for j in range(2):
        medianX.append(med.get_xdata()[j])
        medianY.append(med.get_ydata()[j])
        plt.plot(medianX, medianY, 'k')
        medians[i] = medianY[0]
        # Finally, overplot the sample averages, with horizontal alignment
        # in the center of each box
        plt.plot([np.average(med.get_xdata())], [np.average(datas[i])],
                 color='w', marker='*', markeredgecolor='k')

realname = ['ddd', 'Reconstruction - first', 'Our method - general']
i = 1
for b in bp['boxes']:
    lab = ax.get_xticklabels()[i].get_text()
    print("Label property of box {0} is {1}".format(i, lab))
    b.set_color(res_label_color[lab])
    b.set_label(realname[i])
    i += 1
    if i == 3:
        break

leg = ax.legend()

# https://stackoverflow.com/questions/13828246/matplotlib-text-color-code-in-the-legend-instead-of-a-line
for l in leg.legendHandles:
    l.set_linewidth(10)
leg.draggable()

plt.axvline(x=1.5, ls='-.')
plt.axvline(x=3.5, ls='-', c='black')

x1 = [1, 2.5, 4.5]
squad = ['Expert Error', 'Computation Error', "Combined Expert &\nComputation Error"]

ax.set_xticks(x1)
ax.set_xticklabels(squad, minor=False)

# Hide these grid behind plot objects
ax.set_axisbelow(True)
ax.text(.3, 1, 'Error Measured with Known Ground Truth',
        horizontalalignment='center',
        transform=ax.transAxes
        )

ax.text(.8, 1, 'TRE',
        horizontalalignment='center',
        transform=ax.transAxes
        )
# ax.set_xlabel('')
ax.set_ylabel('Error (in pixels)')

plt.tight_layout()
plt.show()
