# boxplot_jupyter

## How to generate the same boxplot as `box_del32.pdf`?
* Need to process allPoint.txt to get useful points into an excel, eg. `/per_point/Per point (1).xls`. I also used excel to get scatter plot. I tried boxplot in excel directly, but too simple functions. So I go for python.
* Then I goto `/notebooks/docs/tonyTest.ipynb`, here I used simpleElastix to do elastix and transformix. It works partialy. Then I do matplot to draw boxplot with preprocessed data in jupyter notebook after simpleElastix. Easy to experiement. Though need to learn more Python syntax.