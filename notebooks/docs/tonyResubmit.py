import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon
import matplotlib.patches as mpatches

plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 12
plt.rcParams['xtick.labelsize'] = 12

def rotateLabel(ax, deg):
    l = ax.get_xticklabels()
    plt.setp(l, rotation=deg)

# https://convert.town/column-to-comma-separated-list

B1_tra = np.array(
    [14.538,17.408,14.138,17.971,16.634,16.96,13.573,14.617,14.041,14.479,16.783,20.85,18.764,23.317])
B1_tps = np.array(
    [15.067,11.446,5.7446,16.401,15.937,18.466,9.8995,10.488,7.2801,10.247,11.576,5,9.8995,24.556])
B2_tra = np.array(
    [8.9518,4.5959,16.137,33.089,23.808,19.525,26.705,30.473,30.492,37.558,23.308])
B2_tps = np.array(
    [11.533,12.806,11.533,24.617,8.8318,4.899,7.5498,11.916,3.1623,7.8102,1.4142])
B3_tra = np.array(
    [16.512,13.508,20.773,30.233,25.328,25,35.269,25.734,31.311,37.601,30.157,28.141])
B3_tps = np.array(
    [12.41,14.697,7.1414,21.471,5.099,9.6954,11.225,5.4772,9.0554,8.3066,8.4853,8.7178,7.1414])
B4_tra = np.array(
    [25.338,23.363,25.539,15.853,15.01,26.336,22.619,27.114,39.111,25.364,30.1])
B4_tps = np.array(
    [21.61,3.3166,10.724,9.434,5.9161,9.6954,6.7823,14.177,8.6023,13.784,9.8995,5.7446])
B5_tra = np.array(
    [24.301,17.833,25.803,24.298,29.696,17.669,25.82,13.979,13.461,34.077,18.952,9.6219,25.725,23.966])
B5_tps = np.array(
    [8.2462,4.5826,5.4772,3.3166,4.2426,13.077,7.0711,9.1104,3,5.099,5.831,18.439,4.2426,9.1652])

B_label = ('tradition', 'tps','tradition', 'tps','tradition', 'tps','tradition', 'tps','tradition', 'tps')
B_label_color = {'tradition': 'darkkhaki', 'tps': 'royalblue'}
B1_data = [B1_tra, B1_tps]
B2_data = [B2_tra, B2_tps]
B3_data = [B3_tra, B3_tps]
B4_data = [B4_tra, B4_tps]
B5_data = [B5_tra, B5_tps]
B_datas = [B1_tra, B1_tps, B2_tra, B2_tps, B3_tra, B3_tps, B4_tra, B4_tps, B5_tra, B5_tps]

fig = plt.figure()
plt.subplots_adjust(hspace=0.01)

ax = plt.subplot2grid((1,1), (0,0))

bp = ax.boxplot(B_datas, labels = B_label, showfliers=False)
i=0
for b in bp['boxes']:
    lab = ax.get_xticklabels()[i].get_text()
    print("Label property of box {0} is {1}".format(i, lab))
    b.set_color(B_label_color[lab])
    b.set_label(lab)
    i += 1
    if i == 2:
        break

# https://matplotlib.org/examples/pylab_examples/boxplot_demo2.html
# Now fill the boxes with desired colors

boxColors = ['darkkhaki', 'royalblue']
numBoxes = 5*2
medians = list(range(numBoxes))
for i in range(numBoxes):
    box = bp['boxes'][i]
    boxX = []
    boxY = []
    for j in range(5):
        boxX.append(box.get_xdata()[j])
        boxY.append(box.get_ydata()[j])
    boxCoords = list(zip(boxX, boxY))
    # Alternate between Dark Khaki and Royal Blue
    k = i % 2
    boxPolygon = Polygon(boxCoords, facecolor=boxColors[k])
    ax.add_patch(boxPolygon)
    # Now draw the median lines back over what we just filled in
    med = bp['medians'][i]
    medianX = []
    medianY = []
    for j in range(2):
        medianX.append(med.get_xdata()[j])
        medianY.append(med.get_ydata()[j])
        plt.plot(medianX, medianY, 'k')
        medians[i] = medianY[0]
    # Finally, overplot the sample averages, with horizontal alignment
    # in the center of each box
    # plt.plot([np.average(med.get_xdata())], [np.average(data[i])],
    #          color='w', marker='*', markeredgecolor='k')


plt.axvline(x=2.5, ls='-.')
plt.axvline(x=4.5, ls='-.')
plt.axvline(x=6.5, ls='-.')
plt.axvline(x=8.5, ls='-.')
plt.axvline(x=10.5, ls='-.')

# plt.setp(ax.get_xticklabels(),visible=False)


x1 = [1.5,3.5,5.5,7.5, 9.5]
squad = ['Brain1','Brain2','Brain3','Brain4', 'Brain5']

ax.set_xticks(x1)
ax.set_xticklabels(squad, minor=False)
ax.legend()

plt.tight_layout()
plt.show()